# Kata: Vending Machine

##Pre-requisites to work on local environment
The following needs to be installed in order to be able to build and run the tests locally:
* [Java JDK 8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) 
* [Maven](https://maven.apache.org/download.cgi)


### Build

Run `mvn clean install` for building

### Testing

Running `mvn test` will run the unit tests

## Observations

* The vending machine is the application main class and it was designed to be used as Bean for any project, therefore it doesn't have a Main method.
* Only the last requirement is missing due to lack of time(Exact Change Only)
* I made the assumption the test is not looking for UI, that's why the packaging is jar and not war. Anyway it would be easy to integrate with any UI since the Vending Machine is a Bean exposing public methods to perform each action.
* In the test package will be find the unit tests for each class and also a class called IntegrationTests which contains some of the scenarios described in the requirements and how the vending machine behaves.
