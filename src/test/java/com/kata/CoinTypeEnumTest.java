package com.kata;

import org.junit.Test;

import static org.junit.Assert.*;

public class CoinTypeEnumTest {

    @Test
    public void getTypeFromWeightAndSizeShouldReturnNickel(){

        assertEquals(CoinTypeEnum.NICKEL, CoinTypeEnum.getTypeFromWeightAndSize(1,5));
    }

    @Test
    public void getTypeFromWeightAndSizeShouldReturnQuarter(){

        assertEquals(CoinTypeEnum.QUARTER, CoinTypeEnum.getTypeFromWeightAndSize(2,6));
    }

    @Test
    public void getTypeFromWeightAndSizeShouldReturnDime(){

        assertEquals(CoinTypeEnum.DIME, CoinTypeEnum.getTypeFromWeightAndSize(3,7));
    }

    @Test
    public void getTypeFromWeightAndSizeShouldReturnUnknown(){

        assertEquals(CoinTypeEnum.UNKNOWN, CoinTypeEnum.getTypeFromWeightAndSize(1,6));
        assertEquals(CoinTypeEnum.UNKNOWN, CoinTypeEnum.getTypeFromWeightAndSize(2,5));
        assertEquals(CoinTypeEnum.UNKNOWN, CoinTypeEnum.getTypeFromWeightAndSize(5,9));
    }

    @Test
    public void getCorrectPriceValueForNickel(){

        CoinTypeEnum type = CoinTypeEnum.NICKEL;
        assertEquals(new Integer(5), type.getValue());
    }

    @Test
    public void getCorrectPriceValueForDime(){

        CoinTypeEnum type = CoinTypeEnum.DIME;
        assertEquals(new Integer(10), type.getValue());
    }

    @Test
    public void getCorrectPriceValueForQuarter(){

        CoinTypeEnum type = CoinTypeEnum.QUARTER;
        assertEquals(new Integer(25), type.getValue());
    }

    @Test
    public void getDefaultPriceValue(){

        CoinTypeEnum type = CoinTypeEnum.UNKNOWN;
        assertEquals(new Integer(0), type.getValue());
    }
}
