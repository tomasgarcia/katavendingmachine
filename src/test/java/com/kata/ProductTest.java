package com.kata;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ProductTest {

    Product product;

    @Before
    public void setUp(){
        product = new Product(55, "TESTING", 3465);
    }

    @Test
    public void validateProductInitialization(){

        assertEquals(new Integer(55), product.getId());
        assertEquals("TESTING", product.getName());
        assertEquals(new Integer(3465), product.getPrice());
    }

}