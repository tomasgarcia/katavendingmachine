package com.kata;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import static com.kata.ProductConstants.ID_COLA;
import static com.kata.ProductConstants.ID_CHIPS;
import static com.kata.ProductConstants.ID_CANDY;

public class ProductCatalogTest {

    IProductCatalog productCatalog;

    @Before
    public void setUp(){
        productCatalog = new ProductCatalog();
    }

    @Test
    public void validateProductsAfterInitializedAndTheirPrice(){

        assertEquals(new Integer(100), productCatalog.getProduct(ID_COLA).getPrice());
        assertEquals(new Integer(50), productCatalog.getProduct(ID_CHIPS).getPrice());
        assertEquals(new Integer(65), productCatalog.getProduct(ID_CANDY).getPrice());
        assertNull(productCatalog.getProduct(55));
    }
}
