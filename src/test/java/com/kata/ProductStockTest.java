package com.kata;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import static com.kata.ProductConstants.ID_COLA;
import static com.kata.ProductConstants.ID_CHIPS;
import static com.kata.ProductConstants.ID_CANDY;

public class ProductStockTest {

    private ProductStock productStock;

    @Before
    public void setUp(){
        productStock = new ProductStock();
    }

    @Test
    public void validateProductsStatusAfterInitialized(){

        assertFalse(productStock.isProductAvailable(ID_COLA));
        assertFalse(productStock.isProductAvailable(ID_CHIPS));
        assertFalse(productStock.isProductAvailable(ID_CANDY));
        assertFalse(productStock.isProductAvailable(55));
    }

    @Test
    public void isProductAvailableShouldTrueWhenThereIsStockForIt(){

        productStock.addProductToStock(ID_COLA);
        productStock.addProductToStock(ID_CHIPS);

        assertTrue(productStock.isProductAvailable(ID_COLA));
        assertTrue(productStock.isProductAvailable(ID_CHIPS));
        assertFalse(productStock.isProductAvailable(ID_CANDY));
        assertFalse(productStock.isProductAvailable(55));
    }

    @Test
    public void addProductToStockShouldAddItemWhenThereWasNoneInStock(){

        assertFalse(productStock.isProductAvailable(ID_COLA));
        productStock.addProductToStock(ID_COLA);
        assertTrue(productStock.isProductAvailable(ID_COLA));
    }

    @Test
    public void addProductToStockShouldAddItemWhenItemWasAlreadyInStock(){

        productStock.addProductToStock(ID_COLA);
        assertEquals(new Integer(1), productStock.getProductAmount(ID_COLA));
        productStock.addProductToStock(ID_COLA);
        assertEquals(new Integer(2), productStock.getProductAmount(ID_COLA));
    }

    @Test
    public void decreaseOneProductInStockShouldDecreaseTheItemInStockWhenExistsInStock(){

        productStock.addProductToStock(ID_COLA);
        assertEquals(new Integer(1), productStock.getProductAmount(ID_COLA));
        productStock.decreaseOneProductInStock(ID_COLA);
        assertEquals(new Integer(0), productStock.getProductAmount(ID_COLA));
    }

    @Test
    public void decreaseOneProductInStockShouldNotDecreaseTheItemInStockWhenThereWereZeroItemsInStock(){

        productStock.addProductToStock(ID_COLA);
        assertEquals(new Integer(1), productStock.getProductAmount(ID_COLA));
        productStock.decreaseOneProductInStock(ID_COLA);
        productStock.decreaseOneProductInStock(ID_COLA);
        assertEquals(new Integer(0), productStock.getProductAmount(ID_COLA));
    }

    @Test
    public void decreaseOneProductInStockShouldNotDecreaseTheItemInStockWhenItDidNotExistInStock(){

        assertNull(productStock.getProductAmount(ID_COLA));
        productStock.decreaseOneProductInStock(ID_COLA);
        assertNull(productStock.getProductAmount(ID_COLA));
    }

}