package com.kata;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MachineDisplayTest {

    MachineDisplay md;

    @Before
    public void setUp(){
        md = new MachineDisplay();
    }

    @Test
    public void shouldInitializeWithDefaultMessage(){

        assertEquals(MachineDisplay.DEFAULT_MESSAGE, md.getMessage());
    }

    @Test
    public void displayShouldUpdateSpecificMessage(){

        assertEquals(MachineDisplay.DEFAULT_MESSAGE, md.getMessage());

        md.setMessage("OTHER MESSAGE");
        assertEquals("OTHER MESSAGE", md.getMessage());
    }

    @Test
    public void displayMessageShouldBeDefault(){

        md.setDefaultMessage();
        assertEquals(MachineDisplay.DEFAULT_MESSAGE, md.getMessage());
    }

    @Test
    public void displayMessageShouldDisplayCorrectAmount(){

        md.displayAmount(new Integer(534));
        assertEquals("AVAILABLE: $5.34", md.getMessage());
    }

    @Test
    public void displayMessageShouldDisplaySoldOut(){

        assertEquals(MachineDisplay.DEFAULT_MESSAGE, md.getMessage());
        md.displaySoldOutMessage();
        assertEquals("SOLD OUT", md.getMessage());
    }

}