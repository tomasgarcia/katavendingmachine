package com.kata;

import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.*;

public class CoinTest {

    Coin coin;

    @Test
    public void shouldCreateCointAndAssignCorrectValues(){

        coin = new Coin(5, 6);

        assertEquals(5, coin.getWeight());
        assertEquals(6, coin.getSize());
        assertNotNull(coin.getType());
    }
}
