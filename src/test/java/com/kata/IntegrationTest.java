package com.kata;

import org.junit.Before;
import org.junit.Test;

import static com.kata.ProductConstants.ID_CANDY;
import static com.kata.ProductConstants.ID_CHIPS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.junit.Assert.*;


/*
 * All the listed tests below have the assumption that coin's
 * weight and size are:
 *
 * NICKEL weight == 1 && size == 5
 * DIME weight == 3 && size == 7
 * QUARTER weight == 2 && size == 6
 */
public class IntegrationTest {

    private ICoinValidator coinValidator;
    private IMachineDisplay md;
    private ProductStock productStock;
    private ProductCatalog productCatalog;
    private CoinStock coinStock;

    VendingMachine vm;

    @Before
    public void setUp(){
        vm = new VendingMachine();
        coinValidator = spy(CoinValidator.class);
        vm.setCoinValidator(coinValidator);
        md = spy(MachineDisplay.class);
        vm.setMachineDisplay(md);
        coinStock = spy(CoinStock.class);
        vm.setCoinStock(coinStock);
        productStock = spy(ProductStock.class);
        vm.setProductStock(productStock);
        productCatalog = spy(ProductCatalog.class);
        vm.setProductCatalog(productCatalog);
        vm = spy(vm);
    }


    /*
     * Scenario:
     *
     * Given a customer who introduces the following coins: 2 quarters, 3 dimes and 3 nickels
     * When customer select a Candy Product
     * Then he get the product
     *    And the machine displays "thank you" message
     *    And get the change
     *
     * Assumptions:
     *
     * Candy product is available
     * Coins stock is zero
     */
    @Test
    public void customerBuysAProductAndGetTheProductAndChange(){

        // Pre-conditions
        productStock.addProductToStock(ID_CANDY);

        // Given a customer who introduces the following coins: 2 quarters, 3 dimes and 3 nickels
        vm.insertedCoin(2,6); // QUARTER
        vm.insertedCoin(2,6); // QUARTER
        vm.insertedCoin(3,7); // DIME
        vm.insertedCoin(3,7); // DIME
        vm.insertedCoin(3,7); // DIME
        vm.insertedCoin(1,5); // NICKEL
        vm.insertedCoin(1,5); // NICKEL
        vm.insertedCoin(1,5); // NICKEL

        // Validate all the coins have been successfully processed
        assertEquals(new Integer(95), vm.getCustomerMoney());
        verify(coinValidator,times(8)).isValid(any(Coin.class));
        verify(coinStock, times(2)).addCoinToStock(CoinTypeEnum.QUARTER);
        verify(coinStock, times(3)).addCoinToStock(CoinTypeEnum.DIME);
        verify(coinStock, times(3)).addCoinToStock(CoinTypeEnum.NICKEL);
        verify(md,times(8)).displayAmount(anyInt());

        // When customer select a Candy Product
        vm.processProductRequest(ProductConstants.ID_CANDY);

        // Then Gets the product And Gets the change
        verify(productStock, times(1)).decreaseOneProductInStock(ID_CANDY);
        verify(md, times(1)).setMessage("THANK YOU");
        verify(vm, times(1)).makeChange(anyInt());
        verify(vm, times(2)).discardCoin(any(Coin.class));
        assertEquals(new Integer(0), vm.getCustomerMoney());
    }

    /*
     * Scenario:
     *
     * Given a customer who introduces the following coins: 1 quarter and 2 dimes
     * When customer selects "Chips" Product
     * Then the display shows price message
     *    And the display shows Customer Money
     *
     * Assumptions:
     *
     * Chips product is available
     * Coins stock is zero
     */
    @Test
    public void customerDoesNotInsertEnoughMoneyToGetTheProduct(){

        // Pre-conditions
        productStock.addProductToStock(ID_CHIPS);

        // Given a customer who introduces the following coins: 1 quarters and 2 dimes
        vm.insertedCoin(2,6); // QUARTER
        vm.insertedCoin(3,7); // DIME
        vm.insertedCoin(3,7); // DIME

        // Validate all the coins have been successfully processed
        assertEquals(new Integer(45), vm.getCustomerMoney());

        // When customer selects "Chips" Product
        vm.processProductRequest(ID_CHIPS);

        // Then the display shows price message And the display shows Customer Money
        verify(md, times(1)).displayPrice(anyInt());
        verify(md, times(4)).displayAmount(anyInt());
        verify(vm, times(0)).makeChange(anyInt());
        verify(vm, times(0)).discardCoin(any(Coin.class));
        verify(productStock, times(0)).decreaseOneProductInStock(anyInt());
        verify(coinStock, times(0)).decreaseOneCoinInStock(any(CoinTypeEnum.class));
    }


    /*
     * Scenario:
     *
     * Given a customer who introduces the following coins: 2 quarters and 1 dime and 1 nickel
     * When customer selects "Candy" Product
     * Then the display shows "sold out" message
     *    And the display shows Customer Money
     *
     * Assumptions:
     *
     * Candy product is not available
     * Coins stock is zero
     */
    @Test
    public void customerSelectAProductNotAvaialableTheMachineDisplaysSoldOut(){


        // Given a customer who introduces the following coins: 1 quarters and 2 dimes
        vm.insertedCoin(2,6); // QUARTER
        vm.insertedCoin(2,6); // QUARTER
        vm.insertedCoin(3,7); // DIME
        vm.insertedCoin(1,5); // NICKEL

        // Validate all the coins have been successfully processed
        assertEquals(new Integer(65), vm.getCustomerMoney());

        // When customer selects "Chips" Product
        vm.processProductRequest(ID_CANDY);

        // Then the display shows price message And the display shows Customer Money
        verify(md, times(1)).displaySoldOutMessage();
        verify(md, times(5)).displayAmount(anyInt());
        verify(vm, times(0)).makeChange(anyInt());
        verify(vm, times(0)).discardCoin(any(Coin.class));
        verify(productStock, times(0)).decreaseOneProductInStock(anyInt());
    }

    /*
     * Scenario:
     *
     * Given a customer who introduces the following coins: 1 quarter, 1 dime and 2 nickels
     * When customer selects "Return coins"
     * Then the machine returns the inserted coins
     *    And the display shows Customer Money
     *
     * Assumptions:
     * Coins stock is zero
     */
    @Test
    public void customerRequestReturnCoins(){


        // Given a customer who introduces the following coins: 1 quarters and 2 dimes
        vm.insertedCoin(2,6); // QUARTER
        vm.insertedCoin(3,7); // DIME
        vm.insertedCoin(1,5); // NICKEL
        vm.insertedCoin(1,5); // NICKEL

        // Validate all the coins have been successfully processed
        assertEquals(new Integer(45), vm.getCustomerMoney());

        // When customer selects "Chips" Product
        vm.returnCoins();

        // Then the display shows price message And the display shows Customer Money
        verify(vm, times(4)).discardCoin(any(Coin.class));
        verify(vm, times(0)).makeChange(anyInt());
        verify(coinStock, times(4)).decreaseOneCoinInStock(any(CoinTypeEnum.class));
        verify(productStock, times(0)).decreaseOneProductInStock(anyInt());
        verify(md, times(1)).setDefaultMessage();
    }
}
