package com.kata;

import org.hamcrest.core.IsCollectionContaining;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class CoinStockTest {

    CoinStock coinStock;

    @Before
    public void setUp(){
        coinStock = new CoinStock();
    }

    @Test
    public void shouldIniatlizedWithThreeCoinTypes(){

        assertTrue(coinStock.getStock().containsKey(CoinTypeEnum.NICKEL));
        assertTrue(coinStock.getStock().containsKey(CoinTypeEnum.DIME));
        assertTrue(coinStock.getStock().containsKey(CoinTypeEnum.QUARTER));
    }

    @Test
    public void shouldReturnZeroCoinsForSpecificAmountWhenHasNoCoins(){

        Integer amount = 40;

        List<Coin> result = coinStock.getCoinsForSpecificAmount(amount);

        assertEquals(0, result.size());
    }

    @Test
    public void shouldReturnCoinsForSpecificAmountWhenHasCoins() {

        Integer amount = 115;

        coinStock.getStock().put(CoinTypeEnum.NICKEL, 5);
        coinStock.getStock().put(CoinTypeEnum.DIME, 5);
        coinStock.getStock().put(CoinTypeEnum.QUARTER, 5);

        List<Coin> result = coinStock.getCoinsForSpecificAmount(amount);

        assertEquals(6, result.size());
        assertThat(result, IsCollectionContaining.hasItem(new Coin(CoinTypeEnum.NICKEL)));
        assertThat(result, IsCollectionContaining.hasItem(new Coin(CoinTypeEnum.DIME)));
        assertThat(result, IsCollectionContaining.hasItem(new Coin(CoinTypeEnum.QUARTER)));
    }

    @Test
    public void shouldReturnCoinsForSpecificAmountWhenHasCoins_EvenWhenItNeedsToUseSmallerOnes() {

        Integer amount = 95;

        coinStock.getStock().put(CoinTypeEnum.NICKEL, 5);
        coinStock.getStock().put(CoinTypeEnum.DIME, 3);
        coinStock.getStock().put(CoinTypeEnum.QUARTER, 2);

        List<Coin> result = coinStock.getCoinsForSpecificAmount(amount);

        assertEquals(8, result.size());
        assertThat(result, IsCollectionContaining.hasItem(new Coin(CoinTypeEnum.NICKEL)));
        assertThat(result, IsCollectionContaining.hasItem(new Coin(CoinTypeEnum.DIME)));
        assertThat(result, IsCollectionContaining.hasItem(new Coin(CoinTypeEnum.QUARTER)));

        int checker = 0;
        for(Coin c : result){
            checker+=c.getType().getValue();
        }

        assertTrue(amount == checker);
    }

    @Test
    public void shouldAddCoinToStock(){

        assertEquals(new Integer(0), coinStock.getStock().get(CoinTypeEnum.QUARTER));
        assertEquals(new Integer(0), coinStock.getStock().get(CoinTypeEnum.DIME));

        coinStock.addCoinToStock(CoinTypeEnum.QUARTER);

        assertEquals(new Integer(1), coinStock.getStock().get(CoinTypeEnum.QUARTER));
        assertEquals(new Integer(0), coinStock.getStock().get(CoinTypeEnum.DIME));

        coinStock.addCoinToStock(CoinTypeEnum.DIME);

        assertEquals(new Integer(1), coinStock.getStock().get(CoinTypeEnum.QUARTER));
        assertEquals(new Integer(1), coinStock.getStock().get(CoinTypeEnum.DIME));

    }

    @Test
    public void shouldDecreaseCoinInStock(){

        assertEquals(new Integer(0), coinStock.getStock().get(CoinTypeEnum.QUARTER));
        coinStock.addCoinToStock(CoinTypeEnum.QUARTER);

        assertEquals(new Integer(1), coinStock.getStock().get(CoinTypeEnum.QUARTER));

        coinStock.decreaseOneCoinInStock(CoinTypeEnum.QUARTER);

        assertEquals(new Integer(0), coinStock.getStock().get(CoinTypeEnum.QUARTER));
    }

}