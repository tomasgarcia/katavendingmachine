package com.kata;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.junit.Assert.*;

public class VendingMachineTest {

    @Mock ICoinValidator coinValidator;
    @Mock Coin coin;
    @Mock IMachineDisplay md;
    @Mock ProductStock stock;
    @Mock ProductCatalog catalog;
    @Mock CoinStock coinStock;

    @InjectMocks VendingMachine vm;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldProcessValidCoinAndUpdateCustomerValueAndCallDisplayMessage(){

        when(coin.getType()).thenReturn(CoinTypeEnum.QUARTER);
        when(coinValidator.isValid(any(Coin.class))).thenReturn(Boolean.TRUE);

        vm.processCoin(coin);

        assertEquals(new Integer(25), vm.getCustomerMoney());
        verify(md,times(1)).displayAmount(new Integer(25));
    }

    @Test
    public void shouldProcessInvalidAndCallDiscard(){

        vm = spy(vm);
        when(coin.getType()).thenReturn(CoinTypeEnum.UNKNOWN);
        when(coinValidator.isValid(any(Coin.class))).thenReturn(Boolean.FALSE);
        vm.processCoin(coin);

        verify(vm,times(1)).discardCoin(any(Coin.class));
    }

    @Test
    public void shouldCallDisplaySoldOutMessageAndDisplayAmountMessage(){

        Product product = mock(Product.class);
        when(catalog.getProduct(any(Integer.class))).thenReturn(product);
        when(stock.isProductAvailable(anyInt())).thenReturn(Boolean.FALSE);
        vm.setCustomerMoney(525);
        vm.processProductRequest(2);

        verify(md, times(1)).displaySoldOutMessage();
        verify(md, times(1)).displayAmount(vm.getCustomerMoney());
    }

    @Test
    public void shouldProcessProductRequestAndCallDisPlaySoldOutMessageAndDisplayDefaultMessage(){

        Product product = mock(Product.class);
        when(catalog.getProduct(anyInt())).thenReturn(product);
        when(stock.isProductAvailable(anyInt())).thenReturn(Boolean.FALSE);
        vm.setCustomerMoney(0);
        vm.processProductRequest(2);

        verify(md, times(1)).displaySoldOutMessage();
        verify(md, times(1)).setDefaultMessage();
    }

    @Test
    public void shouldProcessProductRequestAndDoNotCallDisplaySoldOutMessageWhenProductIsAvailable(){

        Product product = mock(Product.class);
        when(catalog.getProduct(anyInt())).thenReturn(product);
        when(stock.isProductAvailable(anyInt())).thenReturn(Boolean.TRUE);
        vm.processProductRequest(2);

        verify(md, times(0)).displaySoldOutMessage();
    }

    @Test
    public void shouldProcessProductRequestAndCallDisplaySoldOutMessageWhenProductIsNull(){

        when(catalog.getProduct(any(Integer.class))).thenReturn(null);
        when(stock.isProductAvailable(any(Integer.class))).thenReturn(Boolean.FALSE);
        vm.processProductRequest(2);

        verify(md, times(1)).displaySoldOutMessage();
    }

    @Test
    public void shouldProcessProductRequestAndDeliverTheItemWhenCustomerAmountIsEqualToProductPrice(){

        Product product = mock(Product.class);
        when(product.getPrice()).thenReturn(150);
        when(catalog.getProduct(any(Integer.class))).thenReturn(product);
        when(stock.isProductAvailable(any(Integer.class))).thenReturn(Boolean.TRUE);

        vm.setCustomerMoney(150);
        vm.processProductRequest(2);

        assertEquals(new Integer(0), vm.getCustomerMoney());
        verify(md, times(1)).setMessage("THANK YOU");
    }

    @Test
    public void shouldProcessProductRequestAndDeliverTheItemWhenCustomerAmountIsGreaterThanProductPrice(){

        Product product = mock(Product.class);
        when(product.getPrice()).thenReturn(130);
        when(catalog.getProduct(any(Integer.class))).thenReturn(product);
        when(stock.isProductAvailable(any(Integer.class))).thenReturn(Boolean.TRUE);

        vm.setCustomerMoney(170);
        vm.processProductRequest(2);

        assertEquals(new Integer(0), vm.getCustomerMoney());
        verify(md, times(1)).setMessage("THANK YOU");
    }

    @Test
    public void shouldProcessProductRequestAndDisplayMessageWithProductPriceWhenCustomerAmountisLowerThanProductPrice(){
        Product product = mock(Product.class);
        when(product.getPrice()).thenReturn(170);
        when(catalog.getProduct(any(Integer.class))).thenReturn(product);
        when(stock.isProductAvailable(any(Integer.class))).thenReturn(Boolean.TRUE);

        vm.setCustomerMoney(150);
        vm.processProductRequest(2);

        assertEquals(new Integer(150), vm.getCustomerMoney());
        verify(md, times(1)).displayPrice(product.getPrice());
    }

    @Test
    public void returnCoinsWhenCustomerRequestItCallsCoinStockToGetTheCoins(){

        vm.setCustomerMoney(45);
        vm.returnCoins();
        verify(coinStock, times(1)).getCoinsForSpecificAmount(45);
    }

    @Test
    public void returnCoinsWhenCustomerRequestItCallDiscardCoin(){

        vm = spy(vm);
        vm.setCustomerMoney(65);
        List<Coin> fakeList= new ArrayList<>();
        fakeList.add(new Coin(CoinTypeEnum.QUARTER));
        fakeList.add(new Coin(CoinTypeEnum.QUARTER));
        fakeList.add(new Coin(CoinTypeEnum.DIME));
        fakeList.add(new Coin(CoinTypeEnum.NICKEL));
        when(coinStock.getCoinsForSpecificAmount(anyInt())).thenReturn(fakeList);

        vm.returnCoins();
        verify(coinStock, times(4)).decreaseOneCoinInStock(any(CoinTypeEnum.class));
        verify(vm, times(4)).discardCoin(any(Coin.class));
    }

    @Test
    public void makeChangeWhenProductPriceIsLowerThanCustomerMoney(){

        vm = spy(vm);
        vm.setCustomerMoney(125);

        Product product = mock(Product.class);
        when(product.getPrice()).thenReturn(100);
        when(catalog.getProduct(any(Integer.class))).thenReturn(product);
        when(stock.isProductAvailable(anyInt())).thenReturn(Boolean.TRUE);

        vm.processProductRequest(1);

        verify(vm, times(1)).makeChange(100);
        verify(coinStock, times(1)).getCoinsForSpecificAmount(25);
    }

    @Test
    public void makeChangeIsNotCallWhenProductPriceEqualToCustomerMoney(){

        vm = spy(vm);
        vm.setCustomerMoney(1);

        Product product = mock(Product.class);
        when(product.getPrice()).thenReturn(100);
        when(catalog.getProduct(any(Integer.class))).thenReturn(product);
        when(stock.isProductAvailable(anyInt())).thenReturn(Boolean.TRUE);

        vm.processProductRequest(1);

        verify(vm, times(0)).makeChange(100);
    }

    @Test
    public void makeChangeShouldCallDecreaseStockAmountAndDiscardCoin(){

        vm = spy(vm);
        vm.setCustomerMoney(75);
        List<Coin> fakeList= new ArrayList<>();
        fakeList.add(new Coin(CoinTypeEnum.QUARTER));
        fakeList.add(new Coin(CoinTypeEnum.DIME));
        when(coinStock.getCoinsForSpecificAmount(anyInt())).thenReturn(fakeList);

        vm.makeChange(35);

        verify(vm, times(2)).discardCoin(any(Coin.class));
        verify(coinStock, times(2)).decreaseOneCoinInStock(any(CoinTypeEnum.class));
    }

}
