package com.kata;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.when;
import static org.junit.Assert.*;

public class CoinValidatorTest {

    ICoinValidator coinValidator;
    @Mock Coin coin;

    @Before
    public void setUp(){
        coinValidator = new CoinValidator();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void nickelShouldBeValidCoin(){

        when(coin.getType()).thenReturn(CoinTypeEnum.NICKEL);
        assertTrue(coinValidator.isValid(coin));
    }

    @Test
    public void dimeShouldBeValidCoin(){

        when(coin.getType()).thenReturn(CoinTypeEnum.DIME);
        assertTrue(coinValidator.isValid(coin));
    }

    @Test
    public void quarterShouldBeValidCoin(){

        when(coin.getType()).thenReturn(CoinTypeEnum.QUARTER);
        assertTrue(coinValidator.isValid(coin));
    }

    @Test
    public void pennyShouldNotBeValidCoin(){

        when(coin.getType()).thenReturn(CoinTypeEnum.UNKNOWN);
        assertFalse(coinValidator.isValid(coin));
    }
}
