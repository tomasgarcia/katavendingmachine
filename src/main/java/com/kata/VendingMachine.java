package com.kata;

import javax.inject.Inject;
import java.util.List;

public class VendingMachine {

    @Inject private ICoinValidator coinValidator;
    @Inject private IMachineDisplay display;
    @Inject private IProductStock productStock;
    @Inject private IProductCatalog productCatalog;
    @Inject private ICoinStock coinStock;
    private Integer customerMoney = 0;

    public Integer getCustomerMoney() {
        return customerMoney;
    }

    public void insertedCoin(int weight, int size){
        Coin c = new Coin(weight, size);
        processCoin(c);
    }

    void processCoin(Coin c){
        if(coinValidator.isValid(c)){
            customerMoney += c.getType().getValue();
            coinStock.addCoinToStock(c.getType());
            display.displayAmount(customerMoney);
        }
        else{
            discardCoin(c);
        }
    }

    public void discardCoin(Coin c){
        // Here The machines just return the coin to the customer
    }

    public void processProductRequest(Integer productId){
        Product product = productCatalog.getProduct(productId);
        if(null != product && productStock.isProductAvailable(productId)){
            if(customerMoney >= product.getPrice()){
                display.setMessage("THANK YOU");
                if(customerMoney > product.getPrice()){
                    makeChange(product.getPrice());
                }
                customerMoney = 0;
                productStock.decreaseOneProductInStock(productId);
            }
            else{
                display.displayPrice(product.getPrice());
                displayMessageAccordingCustomerMoney();
            }
        }
        else{
            display.displaySoldOutMessage();
            displayMessageAccordingCustomerMoney();
        }
    }

    public void returnCoins(){
        List<Coin> returnedCoins = coinStock.getCoinsForSpecificAmount(customerMoney);
        returnedCoins.forEach(item->{
            coinStock.decreaseOneCoinInStock(item.getType());
            discardCoin(item);
        });
        display.setDefaultMessage();
    }

    public void makeChange(int price){
        int amount = customerMoney-price;
        List<Coin> returnedCoins = coinStock.getCoinsForSpecificAmount(amount);
        returnedCoins.forEach(item->{
            coinStock.decreaseOneCoinInStock(item.getType());
            discardCoin(item);
        });
        display.setDefaultMessage();
    }

    // This setter was added to enhance the testing scenarios avoiding
    // using reflection to manipulate private fields directly
    protected void setCustomerMoney(Integer amount){
        this.customerMoney = amount;
    }

    private void displayMessageAccordingCustomerMoney(){
        // Real world scenario would need to handle the time interval to ensure the two messages
        // are visualized corretly by the user. To simulate it a Thread.sleep() can be used.
        if(customerMoney > 0) {
            display.displayAmount(customerMoney);
        }
        else{
            display.setDefaultMessage();
        }

    }

    /*
     * The following setter methods should be ONLY used for integration tests purpose
     */
    void setCoinValidator(ICoinValidator coinValidator){
        this.coinValidator = coinValidator;
    }
    void setMachineDisplay(IMachineDisplay md){
        this.display = md;
    }
    void setProductStock(IProductStock productStock){
        this.productStock = productStock;
    }
    void setProductCatalog(IProductCatalog productCatalog){
        this.productCatalog = productCatalog;
    }
    void setCoinStock(ICoinStock coinStock){
        this.coinStock = coinStock;
    }


}
