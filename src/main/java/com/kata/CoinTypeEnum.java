package com.kata;

import java.math.BigDecimal;

/*
 * Assumption: the values to calculate the type are assumed as real ones
 */
public enum CoinTypeEnum {
    NICKEL,
    DIME,
    QUARTER,
    UNKNOWN;

    static CoinTypeEnum getTypeFromWeightAndSize(int weight, int size){
        if(weight == 1 && size == 5){
            return CoinTypeEnum.NICKEL;
        }
        else if(weight == 2 && size == 6){
            return CoinTypeEnum.QUARTER;
        }
        else if(weight == 3 && size == 7){
            return CoinTypeEnum.DIME;
        }
        else{
            return CoinTypeEnum.UNKNOWN;
        }
    }

    public Integer getValue(){
        switch (this){
            case NICKEL: return 5;
            case DIME: return 10;
            case QUARTER: return 25;
            default: return 0;
        }
    }
}
