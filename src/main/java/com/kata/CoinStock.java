package com.kata;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CoinStock implements ICoinStock{

    private Map<CoinTypeEnum, Integer> stock;

    /*
     * The map is intentionally initialized with the correct values
     * so the adding is controlled and we prevent to add an invalid
     * coin.
     * Real container would have an injection of ICoinValidator and
     * load the map dinamically
     */
    public CoinStock(){
        stock = new HashMap<CoinTypeEnum, Integer>();
        stock.put(CoinTypeEnum.NICKEL, 0);
        stock.put(CoinTypeEnum.DIME, 0);
        stock.put(CoinTypeEnum.QUARTER, 0);
    }

    public List<Coin> getCoinsForSpecificAmount(Integer amount){
        List<Coin> result = new ArrayList<Coin>();
        while(amount > 0){
            if(amount >= 25 && stock.get(CoinTypeEnum.QUARTER) > 0){
                result.add(new Coin(CoinTypeEnum.QUARTER));
                amount -= 25;
                stock.put(CoinTypeEnum.QUARTER, stock.get(CoinTypeEnum.QUARTER) - 1);
            }
            else if(amount >= 10 && stock.get(CoinTypeEnum.DIME) > 0){
                result.add(new Coin(CoinTypeEnum.DIME));
                amount -= 10;
                stock.put(CoinTypeEnum.DIME, stock.get(CoinTypeEnum.DIME) - 1);
            }
            else if(amount >= 5 && stock.get(CoinTypeEnum.NICKEL) > 0){
                result.add(new Coin(CoinTypeEnum.NICKEL));
                amount -= 5;
                stock.put(CoinTypeEnum.NICKEL, stock.get(CoinTypeEnum.NICKEL) - 1);
            }
            else{
                // This line should be unreached, in case of any issues it prevents infinitive lop
                break;
            }
        }
        return result;
    }

    public void addCoinToStock(CoinTypeEnum type){
        if(stock.containsKey(type)){
            stock.put(type, stock.get(type) + 1);
        }
    }

    public void decreaseOneCoinInStock(CoinTypeEnum type){
        if(stock.containsKey(type) && stock.get(type) > 0){
            stock.put(type, stock.get(type) - 1);
        }
    }

    protected Map<CoinTypeEnum, Integer> getStock(){
        return stock;
    };

}
