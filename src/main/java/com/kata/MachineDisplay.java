package com.kata;

import java.math.BigDecimal;

public class MachineDisplay implements IMachineDisplay{

    public static final String DEFAULT_MESSAGE = "INSERT COIN";

    private String message;

    public MachineDisplay(){
        setDefaultMessage();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setDefaultMessage(){
        this.message = DEFAULT_MESSAGE;
    }

    public void displayAmount(Integer amount){
        BigDecimal bd = BigDecimal.valueOf(amount/100D);
        bd.setScale(2);
        this.message = "AVAILABLE: $" + bd;
    }

    public void displayPrice(Integer amount) {
        BigDecimal bd = BigDecimal.valueOf(amount/100D);
        bd.setScale(2);
        this.message = "PRICE $" + bd;
    }

    public void displaySoldOutMessage() {
        this.message = "SOLD OUT";
    }
}
