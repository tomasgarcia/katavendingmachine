package com.kata;

public class ProductConstants {

    public static final Integer ID_COLA = 1;
    public static final Integer ID_CHIPS = 2;
    public static final Integer ID_CANDY = 3;
}
