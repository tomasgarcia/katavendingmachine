package com.kata;

import java.util.HashMap;
import java.util.Map;

import static com.kata.ProductConstants.ID_COLA;
import static com.kata.ProductConstants.ID_CHIPS;
import static com.kata.ProductConstants.ID_CANDY;

public class ProductCatalog implements IProductCatalog{

    private Map<Integer, Product> catalog;

    /*
     * The initialization with stubbed values is intended to provide data
     * to work scenarios, real world scenario would require all the logic
     * to get a list of product and create the map, it is considered out of scope
     */
    public ProductCatalog(){
        catalog = new HashMap<Integer, Product>();
        catalog.put(ID_COLA, new Product(ID_COLA, "COLA", 100));
        catalog.put(ID_CHIPS, new Product(ID_CHIPS, "CHIPS", 50));
        catalog.put(ID_CANDY, new Product(ID_CANDY, "CANDY", 65));
    }

    public Product getProduct(Integer productId){
        return catalog.get(productId);
    }
}
