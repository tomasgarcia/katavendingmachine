package com.kata;

import java.util.Objects;

/*
 * In order to prevent the values to be changed directly this class
 * should only provide a constructor to define them and the proper getters
 */
public class Coin {

    private int weight;
    private int size;
    private CoinTypeEnum type;

    public Coin(int weight, int size){
        this.weight = weight;
        this.size = size;
        type = CoinTypeEnum.getTypeFromWeightAndSize(weight, size);
    }

    public Coin(CoinTypeEnum type){
        this.type = type;
    }

    public int getWeight() {
        return weight;
    }

    public int getSize() {
        return size;
    }

    public CoinTypeEnum getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coin coin = (Coin) o;
        return type == coin.type;
    }

    @Override
    public int hashCode() {

        return Objects.hash(type);
    }
}
