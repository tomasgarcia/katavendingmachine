package com.kata;

import java.util.HashMap;
import java.util.Map;


public class ProductStock implements IProductStock{

    private Map<Integer, Integer> stock;
    
    public ProductStock(){
        stock = new HashMap<Integer, Integer>();
    }

    public boolean isProductAvailable(Integer productId){
        return stock.containsKey(productId) && stock.get(productId) > 0;
    }

    public void addProductToStock(Integer productId){
        if(stock.containsKey(productId)){
            stock.put(productId, stock.get(productId) + 1);
        }
        else{
            stock.put(productId, 1);
        }
    }

    public void decreaseOneProductInStock(Integer productId){
        if(stock.containsKey(productId) && stock.get(productId) > 0){
            stock.put(productId, stock.get(productId) - 1);
        }
    }

    Integer getProductAmount(Integer productId){
        return stock.get(productId);
    }
}
