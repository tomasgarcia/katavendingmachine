package com.kata;

public interface ICoinValidator {

    boolean isValid(Coin c);
}
