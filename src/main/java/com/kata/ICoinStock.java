package com.kata;

import java.util.List;

public interface ICoinStock {

    List<Coin> getCoinsForSpecificAmount(Integer amount);
    void addCoinToStock(CoinTypeEnum type);
    void decreaseOneCoinInStock(CoinTypeEnum type);
}
