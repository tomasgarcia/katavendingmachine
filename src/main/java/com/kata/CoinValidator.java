package com.kata;

public class CoinValidator implements ICoinValidator{

    public boolean isValid(Coin c){
        return c != null && c.getType() != CoinTypeEnum.UNKNOWN;
    }
}
