package com.kata;

public interface IProductCatalog {

    Product getProduct(Integer productId);
}
