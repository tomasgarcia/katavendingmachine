package com.kata;

public interface IMachineDisplay {

    String getMessage();
    void setMessage(String message);
    void setDefaultMessage();
    void displayAmount(Integer amount);
    void displayPrice(Integer amount);
    void displaySoldOutMessage();
}
