package com.kata;

public interface IProductStock {

    boolean isProductAvailable(Integer productId);
    void addProductToStock(Integer productId);
    void decreaseOneProductInStock(Integer productId);
}
